import {UpgradeAdapter} from '@angular/upgrade';
import {NgModule, Injectable, Component, forwardRef} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
export const upgradeAdapter = new UpgradeAdapter(forwardRef(() => AppModule));
(<any>window).services = [];
@Injectable()
export class TtService {
    public tt: number = 0;

    constructor() {
        (<any>window).services.push(this);
        console.log('OOOOOOOOOOOOOOOO', this.tt);
    }
}

@Component({
    selector: 'cmp1',
    template: '<div>choooh1</div>'
})
export class Cmp1Component {
    constructor(TtService: TtService) {
        console.log('BUILD CHH1', TtService.tt);
        // TtService.tt = 1111;
    }
}
@NgModule({
    imports: [],
    providers: [TtService],
    declarations: [Cmp1Component],
    exports: [Cmp1Component],
    bootstrap: [],
    // exports: [TtService]
})
export class CommonModule {
}

@NgModule({
    imports: [BrowserModule, CommonModule],
    // providers: [TtService],
    // declarations: [Cmp1Component],
    providers: [],
    // declarations: [],
    exports: [],
    bootstrap: [Cmp1Component]
    // exports: [TtService]
})
export class AppModule {
}
// platformBrowserDynamic().bootstrapModule(AppModule);
@Component({
    selector: 'cmp',
    template: '<div>choooh</div>'
})
class CmpComponent {
    constructor(TtService: TtService) {
        console.log('BUILD', TtService.tt);
        TtService.tt = 6666;
    }
}

@NgModule({
    imports: [BrowserModule, CommonModule],
    providers: [],
    declarations: [
        CmpComponent
    ],
    bootstrap: [CmpComponent],
})
class SecondModule {
}

platformBrowserDynamic().bootstrapModule(SecondModule);