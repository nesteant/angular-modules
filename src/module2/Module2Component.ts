import {Component} from '@angular/core';
import {RootService} from '../root/RootService';
@Component({
    selector: 'mod2',
    template: '<div>MOD2</div>'
})
export class Module2Component {
    constructor(RootService: RootService) {
        console.log('MOD2', RootService.tt);
    }
}