import {NgModule} from '@angular/core';
import {Module2Component} from './Module2Component';
import {BrowserModule} from '@angular/platform-browser';
import {RootService} from '../root/RootService';
@NgModule({
    imports: [],
    providers: [RootService],
    declarations: [Module2Component],
    exports: [],
    bootstrap: [Module2Component]
})
export class Module2Module {
}