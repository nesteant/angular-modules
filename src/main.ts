import {Module1Module} from './module1/Module1Module';
require('reflect-metadata');
require('zone.js/dist/zone');
require('zone.js/dist/long-stack-trace-zone');
require('angular');
import {RootComponent} from './root/RootComponent';
import {RootModule} from './root/RootModule';
import {forwardRef} from '@angular/core';
import {UpgradeAdapter} from '@angular/upgrade';
import {Module1Component} from './module1/Module1Component';


declare let angular: any;
export const upgradeAdapter = new UpgradeAdapter(forwardRef(() => RootModule));
angular.module('rootApp', [])
    .directive('root', upgradeAdapter.downgradeNg2Component(RootComponent))
    .directive('mod1', upgradeAdapter.downgradeNg2Component(Module1Component));
upgradeAdapter.bootstrap(document.getElementById('app'), ['rootApp']);

