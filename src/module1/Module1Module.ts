import {NgModule} from '@angular/core';
import {Module1Component} from './Module1Component';
@NgModule({
    imports: [],
    providers: [],
    declarations: [Module1Component],
    exports: [],
    bootstrap: []
})
export class Module1Module {
}