import {Component, OnInit, Compiler, ComponentFactoryResolver, ViewContainerRef, Injector} from '@angular/core';
import {RootService} from '../root/RootService';
import {Module2Module} from '../module2/Module2Module';
import {Module2Component} from '../module2/Module2Component';
declare let angular: any;

@Component({
    selector: 'mod1',
    template: '<div>MOD1</div>'
})
export class Module1Component implements OnInit {

    constructor(private vc: ViewContainerRef, private injector: Injector, private componentfactoryResolver: ComponentFactoryResolver, RootService: RootService, private Compiler: Compiler) {
        console.log('MOD1', RootService.tt);
    }

    ngOnInit(): void {
        console.log('mod1 init');

        setTimeout(()=> {
            (<any>require).ensure([], (require: any) => {
                console.log('LazyRequire');
                this.Compiler.compileModuleAndAllComponentsAsync(Module2Module).then((ModuleWithComponentFactories)=> {
                    let module = ModuleWithComponentFactories.ngModuleFactory.create(this.injector);
                    var compFactory = module.componentFactoryResolver.resolveComponentFactory(Module2Component);
                    this.vc.createComponent(compFactory);
                });
            });
        }, 500);

    }
}