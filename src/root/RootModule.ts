import {BrowserModule} from '@angular/platform-browser';
import {RootComponent} from './RootComponent';
import {NgModule} from '@angular/core';
import {RootService} from './RootService';
import {Module1Module} from '../module1/Module1Module';
import {Module2Component} from '../module2/Module2Component';
@NgModule({
    imports: [BrowserModule, Module1Module],
    // providers: [TtService],
    // declarations: [Cmp1Component],
    providers: [RootService],
    declarations: [RootComponent],
    exports: [],
    bootstrap: [RootComponent],
    // exports: [TtService]
})
export class RootModule {
}