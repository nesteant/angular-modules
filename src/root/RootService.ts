import {Injectable} from '@angular/core';
@Injectable()
export class RootService {
    public tt: number = 0;

    constructor() {
        this.tt++;
        console.log('RootService', this.tt);
    }
}