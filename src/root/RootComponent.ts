import {Component} from '@angular/core';
import {RootService} from './RootService';
@Component({
    selector: 'root',
    template: '<div>ROOT LOADED</div>'
})
export class RootComponent {
    constructor(RootService: RootService) {
        console.log('BuildRoot');
    }
}