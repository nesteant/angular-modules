var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry: {
        'app': 'src/main'
    },
    output: {
        path: helpers.root('target/dist'),
        filename: 'all.[name].js'
        // library: ['PriceCloud', '[name]'],
        // libraryTarget: 'umd'
    },

    resolve: {
        root: path.resolve(path.join('./')),
        modulesDirectories: [
            path.join(__dirname, '..', 'node_modules')
        ],
        extensions: ['', '.js', '.ts', '.less', '.css'],

        alias: {
            '@angular/upgrade': path.resolve(__dirname, '../node_modules/@angular/upgrade/bundles/upgrade.umd'),
        }
    },

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'ts-loader'
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.css$/,
                exclude: helpers.root('src', 'app'),
                loader: ExtractTextPlugin.extract('style', 'css?sourceMap')
            },
            {
                test: /\.css$/,
                include: helpers.root('src', 'app'),
                loader: 'raw'
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                loader: 'raw-loader!less-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        })
    ]
};